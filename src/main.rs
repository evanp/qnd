#![feature(plugin, decl_macro)]
#![plugin(rocket_codegen)]

extern crate rocket;
extern crate rocket_contrib;
extern crate serde;
extern crate serde_json;

#[macro_use] extern crate serde_derive;

use rocket::Rocket;
use rocket_contrib::Json;
use std::env;
use serde::ser::{Serialize, Serializer, SerializeStruct};

#[derive(Deserialize)]
struct Actor {
    id: String,
    name: String
}

impl Serialize for Actor {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where S: Serializer
    {
        let root = match env::var("ID") {
            Ok(val) => val,
            Err(_e) => "https://qnd.test/".to_string()
        };
        let mut state = serializer.serialize_struct("Actor", 9)?;
        state.serialize_field("@context", "https://www.w3.org/ns/activitystreams")?;
        state.serialize_field("id", &self.id)?;
        state.serialize_field("name", &self.name)?;
        state.serialize_field("type", "Person")?;
        state.serialize_field("inbox", &format!("{}inbox", root))?;
        state.serialize_field("outbox", &format!("{}outbox", root))?;
        state.serialize_field("followers", &format!("{}followers", root))?;
        state.serialize_field("following", &format!("{}following", root))?;
        state.serialize_field("liked", &format!("{}liked", root))?;
        state.end()
    }
}

fn the_actor() -> Actor {
    return Actor {
        id: match env::var("ID") { Ok(val) => val, Err(_e) => "fake".to_string() },
        name: match env::var("NAME") { Ok(val) => val, Err(_e) => "Fake Name".to_string() }
    }
}

#[get("/", format = "application/json")]
fn actor() -> Json<Actor> {
    Json(the_actor())
}

fn qnd() -> Rocket {
    rocket::ignite().mount("/", routes![actor])
}

fn main() {
    qnd().launch();
}

#[cfg(test)]
mod test {
    use super::qnd;
    use rocket::local::Client;
    use rocket::http::{Status, ContentType};
    use serde_json::{from_str, Value};

    #[test]
    fn get_actor() {
        let client = Client::new(qnd()).unwrap();

        // Check that the message exists with the correct contents.
        let mut res = client.get("/").header(ContentType::JSON).dispatch();
        assert_eq!(res.status(), Status::Ok);
        assert_eq!(res.content_type(), Some(ContentType::JSON));
        let bd: String = res.body_string().unwrap();
        let v: Value = from_str(&bd).unwrap();
        assert_eq!(v["id"], "fake");
        assert_eq!(v["name"], "Fake Name");
        assert_eq!(v["type"], "Person");
        assert_eq!(v["@context"], "https://www.w3.org/ns/activitystreams");
        assert_eq!(v["inbox"], "https://qnd.test/inbox");
        assert_eq!(v["outbox"], "https://qnd.test/outbox");
        assert_eq!(v["followers"], "https://qnd.test/followers");
        assert_eq!(v["following"], "https://qnd.test/following");
        assert_eq!(v["liked"], "https://qnd.test/liked");
    }
}
